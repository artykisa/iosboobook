//
//  BookCellModel.swift
//  Library
//
//  Created by Yan Smaliak on 30.11.2020.
//

import UIKit

final class BookCellModel: BookModelType {
    var title: String
    var author: String
    var price: String
    var myBook: String?
    var cover: UIView
    let bookID: BookID
    
    init(book: Book) {
        bookID = book.payload.id
        title = book.payload.name
        author = book.payload.author
        price = "$\(book.payload.price)"
        myBook = book.isOwnedByCurrentUser ? L10n.Localizable.myBook : nil
        if let image = book.image {
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 60, height: 90))
            imageView.layer.cornerRadius = 10
            imageView.image = image.withRoundedCorners(radius: 10)
            let someView = UIView(frame: CGRect(x: 10, y: 10, width: 60, height: 90))
            someView.layer.cornerRadius = 10
            someView.addSubview(imageView)
            cover = someView
        } else {
            let bookCover = BookCover()
            bookCover.baseImage = book.image
            bookCover.title = book.payload.name
            bookCover.backgroundColor = AppColors.coverColor(for: book.payload)
            cover = bookCover
        }
    }
}

extension UIImage {
    // image with rounded corners
    public func withRoundedCorners(radius: CGFloat? = nil) -> UIImage? {
        let maxRadius = min(size.width, size.height) / 2
        let cornerRadius: CGFloat
        if let radius = radius, radius > 0 && radius <= maxRadius {
            cornerRadius = radius
        } else {
            cornerRadius = maxRadius
        }
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        let rect = CGRect(origin: .zero, size: size)
        UIBezierPath(roundedRect: rect, cornerRadius: cornerRadius).addClip()
        draw(in: rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
