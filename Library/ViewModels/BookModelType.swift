//
//  BookModelType.swift
//  Library
//
//  Created by Yan Smaliak on 01.12.2020.
//

import UIKit

protocol BookModelType {
    var title: String { get }
    var author: String { get }
    var price: String { get }
    var myBook: String? { get }
    var cover: UIView { get }
}
