//
//  OrderInfoView.swift
//  Library
//
//  Created by Yan Smaliak on 14.11.2021.
//

import SwiftUI
import SPAlert
import Combine
import RxSwift

struct OrderInfoView: View {
    let repo: CartRepositoryType
    let price: Double
    let bonuses: Double

    private let bag = DisposeBag()

    @State private var city: String = ""
    @State private var street: String = ""
    @State private var house: String = ""
    @State private var flat: String = ""
    @State private var name: String = ""
    @State private var number: String = ""
    @State private var code: String = ""
    @State private var bonusesToUse: String = ""

    var body: some View {
        NavigationView {
            content
                .navigationBarTitleDisplayMode(.large)
                .navigationBarTitle(Text("Order Details"))
        }

    }

    private var content: some View {
        VStack {
            ScrollView {
                VStack(alignment: .leading) {
                    SwiftUI.TextField("City", text: $city)
                    SwiftUI.TextField("Street", text: $street)
                    SwiftUI.TextField("House", text: $house)
                    SwiftUI.TextField("Flat", text: $flat)
                    SwiftUI.TextField("Name", text: $name)
                    SwiftUI.TextField("Phone number", text: $number)
                        .keyboardType(.phonePad)

                    SwiftUI.TextField("Code", text: $code)
                        .keyboardType(.numberPad)
                        .onReceive(Just(code)) { newValue in
                            let filtered = newValue.filter { "0123456789".contains($0) }
                            if filtered != newValue {
                                self.code = filtered
                            }
                        }

                    SwiftUI.TextField("Bonuses (less than \(Int(price * 0.3)))", text: $bonusesToUse)
                        .keyboardType(.numberPad)
                        .onReceive(Just(bonusesToUse)) { newValue in
                            let filtered = newValue.filter { "0123456789".contains($0) }
                            if filtered != newValue {
                                self.bonusesToUse = filtered
                            }
                        }
                }
            }
            .padding(20)

            Spacer()

            SwiftUI.Button(action: {
                pay()
            }, label: {
                ZStack {
                    RoundedRectangle(cornerRadius: 15)
                        .foregroundColor(.black)
                        .frame(height: 50)
                        .padding([.leading, .trailing], 20)
                    Text("Order")
                        .font(.title2)
                        .foregroundColor(.white)
                }
            })
        }
    }

    private func pay() {
        if checkInput() {
            SPAlert.present(title: "Error", message: "Check input", preset: .error)
        } else {
            let order = Order(
                bonuses: Int(bonusesToUse) ?? 0,
                city: city,
                street: street,
                house: house,
                flat: flat,
                name: name,
                number: number,
                code: Int(code) ?? 0
            )
            repo.order(order).asObservable().subscribe().disposed(by: bag)
            SPAlert.present(title: "Success", preset: .done)
        }
    }

    private func checkInput() -> Bool {
        return city.isEmpty || street.isEmpty || house.isEmpty || flat.isEmpty || name.isEmpty || code.isEmpty || (Double(bonusesToUse) ?? 0 > bonuses || Int(bonusesToUse) ?? 0 > Int(price * 0.3))
    }
}

extension String {
    var westernArabicNumeralsOnly: String {
        let pattern = UnicodeScalar("0")..."9"
        return String(unicodeScalars.flatMap { pattern ~= $0 ? Character($0) : nil })
    }
}

struct Order: Encodable {
    let bonuses: Int
    let city: String
    let street: String
    let house: String
    let flat: String
    let name: String
    let number: String
    let code: Int
}
