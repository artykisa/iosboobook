//
//  CartViewModel.swift
//  Library
//
//  Created by Yan Smaliak on 13.09.2021.
//

import Foundation
import RxSwift
import RxCocoa
import RxSwiftUtilities
import UIKit
import SwiftUI

// MARK: - Input

struct CartViewModelInput {
	let refreshTrigger: Driver<Void>
	let selectedCell: Driver<BookCellModel>
    let checkOutButtonTap: Driver<Void>
}

// MARK: - Output

struct CartViewModelOutput {
	let cellModel: Driver<[BookCellModel]>
	let isButtonHidden: Driver<Bool>
	let isLoading: Driver<Bool>
    let booksInCartText: Driver<String>
	let triggers: Driver<Void>
}

// MARK: - Router Type

protocol CartRouterType: BaseRouterType {
	func showDetails(forBookWithID bookID: BookID)
    func showCheckOut(repo: CartRepositoryType, price: Double, bonuses: Double)
}

// MARK: - Router

final class CartRouter: BaseRouter, CartRouterType {
	
	func showDetails(forBookWithID bookID: BookID) {
		let bookDetailsVC = BookDetailsVC()
		let bookDetailsNavigationController = UINavigationController(rootViewController: bookDetailsVC)
		let bookDetailsRouter = BookDetailsRouter(rootViewController: bookDetailsVC)
		let bookDetailsViewModel = BookDetailsViewModel(router: bookDetailsRouter,
														repository: SwinjectContainer.container.resolve(BookRepository.self)!,
														bookID: bookID)
		bookDetailsVC.viewModel = bookDetailsViewModel
		bookDetailsNavigationController.presentationController?.delegate = rootViewController as? UIAdaptivePresentationControllerDelegate
		rootViewController?.present(bookDetailsNavigationController, animated: true)
	}

    func showCheckOut(repo: CartRepositoryType, price: Double, bonuses: Double) {
        let hostingController = UIHostingController(rootView: OrderInfoView(repo: repo, price: price, bonuses: bonuses))
        hostingController.presentationController?.delegate = rootViewController as? UIAdaptivePresentationControllerDelegate
        rootViewController?.present(hostingController, animated: true)
    }
	
}

protocol CartRepositoryType {
	func booksInCart() -> Single<[Book]>
	func booksInCartFromStorage() -> Single<[Book]>
    func order(_ order: Order) -> Single<[Book]>
    func bonuses() -> Single<Double>
}

// MARK: - View Model Type

protocol CartViewModelType {
	func transform(_ input: CartViewModelInput) -> CartViewModelOutput
}

// MARK: - View Model

struct CartViewModel: CartViewModelType {
	// MARK: - Properties
	
	private let router: CartRouterType
	private let repository: CartRepositoryType
	
	// MARK: - Init
	
	init(router: CartRouterType, repository: CartRepositoryType) {
		self.router = router
		self.repository = repository
	}
	
	// MARK: - Public Methods
	
	func transform(_ input: CartViewModelInput) -> CartViewModelOutput {
        var price = 0.0
        var bon = 0.0

		let loadingSubject = ActivityIndicator()
		
		let booksFromStorage = input.refreshTrigger
			.flatMapLatest {
				repository
					.booksInCart()
					.trackActivity(loadingSubject)
                    .asDriver(onErrorDo: router.showError)
			}
		
		let booksFromApi = input.refreshTrigger
			.flatMapLatest {
				repository
					.booksInCartFromStorage()
					.trackActivity(loadingSubject)
					.asDriver(onErrorDo: router.showError)
			}
		
		let booksDriver = Driver
			.merge(booksFromApi, booksFromStorage)
		
		let cellViewModel = booksDriver
			.map { $0.map { book -> BookCellModel in BookCellModel(book: book) } }
		
		let showBookDetails = input.selectedCell
			.map { router.showDetails(forBookWithID: $0.bookID) }
		
		let loadingSubjectDriver = loadingSubject
			.asDriver(onErrorJustReturn: false)

        let bonuses = input.refreshTrigger
            .flatMapLatest {
                repository
                    .bonuses()
                    .asDriver(onErrorDo: router.showError)
            }

        let booksCountText: Driver<String> = Driver
            .combineLatest(booksDriver, bonuses, resultSelector: { ($0, $1) })
            .map { books, bonuses in
                var totalPrice = 0.0
                var totalCount = 0
                for book in books {
                    totalPrice += book.payload.price * Double(book.payload.count)
                    totalCount += book.payload.count
                }
                price = totalPrice
                bon = bonuses
                return "Books in cart: \(totalCount). Price: \(totalPrice). Bonuses: \(bonuses)"
            }
		
		let booksArrayIsEmptyDriver = booksDriver
			.map { $0.isEmpty }
		
		let isLoading = Driver
			.combineLatest(loadingSubjectDriver, booksArrayIsEmptyDriver, resultSelector: { ($0, $1) })
			.map { $0 && $1 }
		
		let isButtonHidden = booksDriver
			.map { $0.isEmpty }

        let checkOut = input.checkOutButtonTap
            .map { _ in router.showCheckOut(repo: repository, price: price, bonuses: bon) }
		
		return .init(
			cellModel: cellViewModel,
			isButtonHidden: isButtonHidden,
			isLoading: isLoading,
            booksInCartText: booksCountText,
            triggers: .merge(showBookDetails, checkOut)
		)
	}
}

