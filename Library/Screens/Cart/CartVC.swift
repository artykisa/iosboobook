//
//  CartVC.swift
//  Library
//
//  Created by Yan Smaliak on 13.09.2021.
//

import UIKit
import RxSwift
import RxCocoa

final class CartVC: UIViewController {
	
	// MARK: - Properties
	
	private let tableView: UITableView = {
		let tableView = UITableView()
		tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.estimatedSectionHeaderHeight = 88
        tableView.sectionHeaderHeight = 45
		tableView.estimatedRowHeight = 88
		tableView.rowHeight = UITableView.automaticDimension
		tableView.tableFooterView = UIView()
		tableView.isScrollEnabled = true
		return tableView
	}()
	
	private let spinner: UIActivityIndicatorView = {
		let spinner = UIActivityIndicatorView()
		spinner.translatesAutoresizingMaskIntoConstraints = false
		spinner.style = .large
		return spinner
	}()

    private let headerView = CartHeaderView()
	
	private let checkOutButton = Button(withTitle: "Check out")
	
	private var disposeBag = DisposeBag()
	
	var viewModel: CartViewModelType? {
		didSet {
			loadViewIfNeeded()
			bindViewModel()
		}
	}
	
	// MARK: - Life Cycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		setupTableView()
		setupLayout()
//        headerView.booksCountLabelText = 
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		if !view.subviews.contains(tableView) {
			embedTableView()
		}
		if !view.subviews.contains(spinner) {
			embedSpinner()
		}
	}
}

// MARK: - TableView Delegate

extension CartVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        headerView
    }
}

// MARK: - UIAdaptivePresentationControllerDelegate

extension CartVC: UIAdaptivePresentationControllerDelegate {
	func presentationControllerDidDismiss(_ presentationController: UIPresentationController) { }
}

// MARK: - Private Extension

private extension CartVC {
	func setupLayout() {
		navigationItem.title = "Cart"
		navigationController?.navigationBar.prefersLargeTitles = true
		view.backgroundColor = .systemBackground
	}
	
	func embedSpinner() {
		view.addSubview(spinner)
		
		NSLayoutConstraint.activate([
			spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor),
			spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor)
		])
	}
	
	func embedTableView() {
		view.addSubview(checkOutButton)
		view.addSubview(tableView)
		
		NSLayoutConstraint.activate([
			checkOutButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
			checkOutButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -10),
			checkOutButton.heightAnchor.constraint(equalToConstant: checkOutButton.defaultHeight),
			checkOutButton.widthAnchor.constraint(equalToConstant: checkOutButton.defaultWidth),
			
			tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
			tableView.bottomAnchor.constraint(equalTo: checkOutButton.topAnchor, constant: -10),
			tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
			tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
		])
	}
	
	func setupTableView() {
		tableView.delegate = self
		tableView.register(cellType: BookCell.self)
	}
	
	func bindViewModel() {
		disposeBag = .init()
		
		let viewWillAppear = rx
			.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
			.asDriver(onErrorDriveWith: .never())
			.mapToVoid()
		
		let modalDismissed = rx
			.sentMessage(#selector(UIAdaptivePresentationControllerDelegate.presentationControllerDidDismiss(_:)))
			.asDriver(onErrorDriveWith: .never())
			.mapToVoid()
		
		guard let output = viewModel?.transform(
            .init(
                refreshTrigger: .merge(viewWillAppear, modalDismissed),
                selectedCell: tableView.rx.modelSelected(BookCellModel.self).asDriver(),
                checkOutButtonTap: checkOutButton.rx.tap.asDriver()
            )
		) else { return }
		
		disposeBag.insert(
			output.cellModel.drive(tableView.rx.items(cellIdentifier: BookCell.reuseIdentifier,
														  cellType: BookCell.self)) { (_, cellViewModel, cell) in
				cell.viewModel = cellViewModel
			},
			output.isButtonHidden.drive(checkOutButton.rx.isHidden),
			output.isLoading.drive(spinner.rx.isAnimating),
            output.booksInCartText.drive(onNext: { [unowned self] in
                headerView.booksCountLabelText = $0
            }),
			output.triggers.drive()
		)
	}
}

