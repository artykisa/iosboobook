//
//  AccountViewModel.swift
//  Library
//
//  Created by Yan Smaliak on 07.12.2020.
//

import Foundation
import RxSwift
import RxCocoa
import RxSwiftUtilities

// MARK: - Input

struct AccountViewModelInput {
    let refreshTrigger: Driver<Void>
    let logoutButton: Driver<Void>
    let selectedCell: Driver<BookCellModel>
}

// MARK: - Output

struct AccountViewModelOutput {
    let emailLabel: Driver<String>
    let cellModel: Driver<[BookCellModel]>
    let isLoading: Driver<Bool>
    let triggers: Driver<Void>
}

// MARK: - Router Type

protocol AccountRouterType: BaseRouterType {
    func showDetails(forBookWithID bookID: BookID)
}

// MARK: - Router

final class AccountRouter: BaseRouter, AccountRouterType {
    
    func showDetails(forBookWithID bookID: BookID) {
        let bookDetailsVC = BookDetailsVC()
        let bookDetailsNavigationController = UINavigationController(rootViewController: bookDetailsVC)
        let bookDetailsRouter = BookDetailsRouter(rootViewController: bookDetailsVC)
        let bookDetailsViewModel = BookDetailsViewModel(router: bookDetailsRouter,
                                                        repository: SwinjectContainer.container.resolve(BookRepository.self)!,
                                                        bookID: bookID)
        bookDetailsVC.viewModel = bookDetailsViewModel
        bookDetailsNavigationController.presentationController?.delegate = rootViewController as? UIAdaptivePresentationControllerDelegate
        rootViewController?.present(bookDetailsNavigationController, animated: true)
    }
    
}

protocol AccountRepositoryType {
//    func bookBeingReadByCurrentUser() -> Single<Book?>
//    func bookBeingReadByCurrentUserFromStorage() -> Single<Book?>
}

// MARK: - View Model Type

protocol AccountViewModelType {
    func transform(_ input: AccountViewModelInput) -> AccountViewModelOutput
}

// MARK: - View Model

struct AccountViewModel: AccountViewModelType {
    // MARK: - Properties
    
    private let router: AccountRouterType
    private let repository: AccountRepositoryType
    private let currentUserEmail: String
    private let logOut: () -> Void
    
    // MARK: - Init
    
    init(router: AccountRouterType, repository: AccountRepositoryType, currentUserEmail: String, logOut: @escaping () -> Void) {
        self.router = router
        self.repository = repository
        self.currentUserEmail = currentUserEmail
        self.logOut = logOut
    }
    
    // MARK: - Public Methods
    
    func transform(_ input: AccountViewModelInput) -> AccountViewModelOutput {
        let loadingSubject = ActivityIndicator()
        
        let emailLabelDriver = Driver.from(optional: currentUserEmail)
        
//        let bookFromStorage = input.refreshTrigger
//            .flatMapLatest {
//                repository
//                    .bookBeingReadByCurrentUserFromStorage()
//                    .trackActivity(loadingSubject)
//                    .asDriver(onErrorDriveWith: .never())
//            }
//
//        let bookFromApi = input.refreshTrigger
//            .flatMapLatest {
//                repository
//                    .bookBeingReadByCurrentUser()
//                    .trackActivity(loadingSubject)
//                    .asDriver(onErrorDo: router.showError)
//            }
        
		let booksDriver: Driver<[Book]> = Driver
			.just([])
        
        let cellViewModel = booksDriver
			.map { $0.map { book -> BookCellModel in
				BookCellModel(book: book)
			}}
        
        let logoutDriver = input.logoutButton
            .do(onNext: {
                logOut()
            })
        
        let showBookDetails = input.selectedCell
            .map { router.showDetails(forBookWithID: $0.bookID) }
        
        let loadingSubjectDriver = loadingSubject
            .asDriver(onErrorJustReturn: false)
        
        let booksArrayIsEmptyDriver = booksDriver
            .map { $0.isEmpty }
        
        let isLoading = Driver
            .combineLatest(loadingSubjectDriver, booksArrayIsEmptyDriver, resultSelector: { ($0, $1) })
            .map { $0 && $1}
        
        return .init(emailLabel: emailLabelDriver,
                     cellModel: cellViewModel,
                     isLoading: isLoading,
                     triggers: .merge(
                        logoutDriver,
                        showBookDetails
                     ))
    }
}
