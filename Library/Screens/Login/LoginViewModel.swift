//
//  LoginVeiwModel.swift
//  Library
//
//  Created by Yan Smaliak on 11/11/20.
//

import Foundation
import RxSwift
import RxCocoa
import UIKit
import RxSwiftUtilities
import RxKeyboard

// MARK: - Input

struct LoginViewModelInput {
    let email: Driver<String?>
    let password: Driver<String?>
    let loginButtonTapped: Driver<Void>
    let signupButtonTapped: Driver<Void>
    let doneTrigger: Driver<Void>
}

// MARK: - Output

struct LoginViewModelOutput {
    let isButtonEnabled: Driver<Bool>
    let isLoading: Driver<Bool>
    let triggers: Driver<Void>
    let endEditing: Driver<Void>
    let keyboardHeight: Driver<CGFloat>
}

// MARK: - Router Type

protocol LoginRouterType: BaseRouterType {
    func logIn()
}

// MARK: - Router

final class LoginRouter: BaseRouter, LoginRouterType {
    private let mainRouter: MainRouter
    
    init(rootViewController: UIViewController, mainRouter: MainRouter) {
        self.mainRouter = mainRouter
        super.init(rootViewController: rootViewController)
    }
    
    func logIn() {
        mainRouter.start()
    }
}

// MARK: - View Model Type

protocol LoginViewModelType {
    func transform(_ input: LoginViewModelInput) -> LoginViewModelOutput
}

// MARK: - View Model

struct LoginViewModel: LoginViewModelType {
    // MARK: - Properties
    
    private let router: LoginRouterType
    private let apiController: ApiControllerType
    private let credentialsStore: CredentialsStoreType
    
    // MARK: - Init
    
    init(router: LoginRouterType, apiController: ApiControllerType, credentialsStore: CredentialsStoreType) {
        self.router = router
        self.apiController = apiController
        self.credentialsStore = credentialsStore
    }
    
    // MARK: - Public Methods
    
    func transform(_ input: LoginViewModelInput) -> LoginViewModelOutput {
        let loadingSubject = ActivityIndicator()
        
        let credentials: Driver<Credentials> = Driver
            .combineLatest(input.email, input.password, resultSelector: { ($0, $1) })
            .flatMapLatest {
                guard let email = $0.0, let password = $0.1 else { return .never() }
                
                let credentials = Credentials(email: email, password: password)
                
                return .just(credentials)
            }
        
        let tryToLogin = input.loginButtonTapped
            .withLatestFrom(credentials)
            .flatMapLatest {
                apiController
                    .newCall(.login(credentials: $0))
                    .do(onSuccess: { data in
						credentialsStore.userCredentials = UserCredentials(
							id: data.id,
							email: data.email,
							token: data.accessToken
						)
                    })
						.do(onError: {
							print($0)
						})
                    .trackActivity(loadingSubject)
                    .asDriver(onErrorDo: router.showError)
                    .mapToVoid()
                    .do(onNext: { router.logIn() })
            }
        
        let tryToSignup = input.signupButtonTapped
            .withLatestFrom(credentials)
            .flatMapLatest {
                apiController
                    .call(.signup(credentials: $0))
                    .do(onSuccess: { data in
                        credentialsStore.userCredentials = UserCredentials(id: data.id,
                                                                           email: data.email,
                                                                           token: data.accessToken)
                    })
                    .trackActivity(loadingSubject)
                    .asDriver(onErrorDo: router.showError)
                    .mapToVoid()
                    .do(onNext: { router.logIn() })
            }
        
        let isLoading = loadingSubject
            .asDriver(onErrorJustReturn: false)
        
        let isButtonEnabled = isLoading
            .map { !$0 }
        
        return .init(isButtonEnabled: isButtonEnabled,
                     isLoading: isLoading,
                     triggers: .merge(
                        tryToLogin,
                        tryToSignup
                     ),
                     endEditing: .merge(
                        input.doneTrigger,
                        input.loginButtonTapped,
                        input.signupButtonTapped
                     ),
                     keyboardHeight: RxKeyboard.instance.visibleHeight.asDriver()
        )
    }
}
