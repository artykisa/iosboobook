//
//  BookDetailsViewModel.swift
//  Library
//
//  Created by Yan Smaliak on 28.11.2020.
//

import Foundation
import RxSwift
import RxCocoa
import RxSwiftUtilities

// MARK: - Input

struct BookDetailsViewModelInput {
    let refreshTrigger: Driver<Void>
    let shareButton: Driver<Void>
    let takeButton: Driver<Void>
    let plusButton: Driver<Void>
    let minusButton: Driver<Void>
}

// MARK: - Output

struct BookDetailsViewModelOutput {
    let isLoading: Driver<Bool>
    let author: Driver<String>
    let uploadedAt: Driver<String>
    let price: Driver<String?>
    let inCartCount: Driver<String?>
    let cover: Driver<UIView>
    let name: Driver<String>
    let description: Driver<String?>
    let buttonTitle: Driver<String>
    let isButtonEnabled: Driver<Bool>
    let isStepperHidden: Driver<Bool>
    let triggers: Driver<Void>
}

// MARK: - Router Type

protocol BookDetailsRouterType: BaseRouterType {
    func shareBook(named name: String, withCover cover: UIImage)
    func showAlert(withMessage message: String, andImage image: UIImage)
}

// MARK: - Router

final class BookDetailsRouter: BaseRouter, BookDetailsRouterType {
    
    func shareBook(named name: String, withCover cover: UIImage) {
        let shareText = L10n.Localizable.heyCheckOutOnLibraryApp(name)
        
        let vc = UIActivityViewController(activityItems: [shareText, cover], applicationActivities: [])
        
        if let popover = vc.popoverPresentationController, let parentVC = rootViewController as? BookDetailsVC {
            popover.barButtonItem = parentVC.shareButton
        }
        
        self.rootViewController?.present(vc, animated: true)
    }
    
}

// MARK: - Book Share Info

struct BookShareInfo {
    var name: String
    var cover: UIView
}

protocol BookDetailsRepositoryType {
    func loadBook(withID bookID: BookID) -> Single<Book>
    func loadBookFromStorage(withID bookID: BookID) -> Single<Book>
    func takeOrReturnBookAction(forBookWithID bookID: BookID, andReaderUserID readerUserID: UserID?) -> Single<Book>
	func addOrRemove(bookWithId bookId: BookID) -> Single<Book>
    func bookBeingReadByCurrentUserFromStorage() -> Single<Book?>
	func isCartContains(bookWithId bookId: BookID) -> Bool
    func add(bookWithId bookId: BookID) -> Single<Book>
    func remove(bookWithId bookId: BookID) -> Single<Book>
}

// MARK: - View Model Type

protocol BookDetailsViewModelType {
    func transform(_ input: BookDetailsViewModelInput) -> BookDetailsViewModelOutput
}

// MARK: - View Model

struct BookDetailsViewModel: BookDetailsViewModelType {
    // MARK: - Properties
    
    private let router: BookDetailsRouterType
    private let repository: BookDetailsRepositoryType
    private let bookID: BookID
    
    // MARK: - Init
    
    init(router: BookDetailsRouterType, repository: BookDetailsRepositoryType, bookID: BookID) {
        self.router = router
        self.repository = repository
        self.bookID = bookID
    }
    
    // MARK: - Public Methods
    
    func transform(_ input: BookDetailsViewModelInput) -> BookDetailsViewModelOutput {
        let loadingSubject = ActivityIndicator()

        let isLoading = loadingSubject
            .asDriver(onErrorJustReturn: false)
        
        let loadBookFromStorage = input.refreshTrigger
            .flatMapLatest {
                repository
                    .loadBookFromStorage(withID: bookID)
                    .asDriver(onErrorDriveWith: .never())
            }

        let takeButtonDriver = input.takeButton
            .flatMapLatest {
                repository
                    .addOrRemove(bookWithId: bookID)
                    .trackActivity(loadingSubject)
                    .asDriver(onErrorDo: router.showError)
            }

        let plusButtonDriver = input.plusButton
            .flatMapLatest {
                repository
                    .add(bookWithId: bookID)
                    .trackActivity(loadingSubject)
                    .asDriver(onErrorDo: router.showError)
            }

        let minusButtonDriver = input.minusButton
            .flatMapLatest {
                repository
                    .remove(bookWithId: bookID)
                    .trackActivity(loadingSubject)
                    .asDriver(onErrorDo: router.showError)
            }
        
        let loadBookFromApi = Driver
            .merge(input.refreshTrigger, takeButtonDriver.mapToVoid(), plusButtonDriver.mapToVoid(), minusButtonDriver.mapToVoid())
            .flatMapLatest {
                repository
                    .loadBook(withID: bookID)
                    .asDriver(onErrorDo: router.showError)
            }
        
        let loadBook = Driver
            .merge(loadBookFromApi, loadBookFromStorage)

        let bookDriver = Driver
            .merge(loadBook, takeButtonDriver)

        let authorDriver: Driver<String> = bookDriver
            .map {
                "Author: \($0.payload.author)"
            }
        
        let uploadedAtDriver: Driver<String> = bookDriver
            .map {
                let formattedDate = DateFormatter.dMy.string(from: $0.payload.createdAt)
                return "Added at \(formattedDate)"
            }
        
        let inCartCount: Driver<String?> = bookDriver
            .map {
                if $0.payload.count > 0 {
                    return "In cart: \($0.payload.count)"
                }

                return nil
            }
        
        let price: Driver<String?> = bookDriver
            .map { "$\($0.payload.price)" }
        
        let coverDriver: Driver<UIView> = bookDriver
            .map { book in
                if let image = book.image {
                    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 180, height: 250))
                    imageView.layer.cornerRadius = 10
                    imageView.image = image.withRoundedCorners(radius: 10)
                    let someView = UIView(frame: CGRect(x: 0, y: 0, width: 180, height: 250))
                    someView.layer.cornerRadius = 10
                    someView.addSubview(imageView)
                    return someView
                } else {
                    let bookCover = BookCover()
                    bookCover.title = book.payload.name
                    bookCover.titleFont = UIFont.preferredFont(forTextStyle: .headline)
                    bookCover.backgroundColor = AppColors.coverColor(for: book.payload)
                    return bookCover
                }
            }
        
        let nameDriver = bookDriver
            .map { $0.payload.name }

        let description = bookDriver
            .map { $0.payload.description }
        
        let takeButtonTitleDriver = bookDriver
			.map { repository.isCartContains(bookWithId: $0.payload.id) ? "Remove from cart" : "Add to cart" }
        
        let bookShareInfoDriver: Driver<BookShareInfo> = Driver
            .combineLatest(nameDriver, coverDriver, resultSelector: { ($0, $1) })
            .flatMapLatest {
                .just(BookShareInfo(name: $0.0, cover: $0.1))
            }
        
        let shareButtonDriver = input.shareButton
            .withLatestFrom(bookShareInfoDriver)
            .do(onNext: { bookShareInfo in
                // swiftlint:disable force_cast
                let cover = bookShareInfo.cover as! BookCover
                // swiftlint:enable force_cast
                
                cover.toImage(completion: { coverImage in
                    router
                        .shareBook(named: bookShareInfo.name, withCover: coverImage)
                })
            })
            .mapToVoid()
        
        let didShowAlert = takeButtonDriver
            .map { book -> (String, UIImage) in
                let title = repository.isCartContains(bookWithId: book.payload.id) ? "Added" : "Removed"
                let image = repository.isCartContains(bookWithId: book.payload.id) ?
                    UIImage(systemName: "tray.and.arrow.down") : UIImage(systemName: "tray.and.arrow.up")
                
                return (title, image!)
            }
            .do(onNext: {
                router.showAlert(withMessage: $0.0, andImage: $0.1)
            })
            .mapToVoid()

        let isStepperHidden = bookDriver
            .map { !repository.isCartContains(bookWithId: $0.payload.id) }
        
        let isButtonEnabled = Driver
                .just(true)

        return .init(isLoading: isLoading,
                     author: authorDriver,
                     uploadedAt: uploadedAtDriver,
                     price: price,
                     inCartCount: inCartCount,
                     cover: coverDriver,
                     name: nameDriver,
                     description: description,
                     buttonTitle: takeButtonTitleDriver,
                     isButtonEnabled: isButtonEnabled,
                     isStepperHidden: isStepperHidden,
                     triggers: .merge(
                        shareButtonDriver,
                        didShowAlert
                     )
        )
    }
    
    // MARK: - Private Methods
    
    private func canCurrentUserInteractWithBook(_ book: Book) -> Single<Bool> {
		.just(true)
    }
}
