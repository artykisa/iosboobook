//
//  BookDetailsVC.swift
//  Library
//
//  Created by Yan Smaliak on 27.11.2020.
//

import UIKit
import RxSwift
import RxCocoa

final class BookDetailsVC: UIViewController {
    
    // MARK: - Properties
    
    public let shareButton: UIBarButtonItem = {
        let button = UIBarButtonItem(systemItem: .action)
        button.tintColor = .label
        return button
    }()
    
    private let bookCoverContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private var bookCover: UIView? {
        didSet {
            oldValue?.removeFromSuperview()
        }
        willSet {
            bookCoverContainer.addSubview(newValue!)
            
            NSLayoutConstraint.activate([
                newValue!.topAnchor.constraint(equalTo: bookCoverContainer.topAnchor),
                newValue!.leadingAnchor.constraint(equalTo: bookCoverContainer.leadingAnchor),
                newValue!.trailingAnchor.constraint(equalTo: bookCoverContainer.trailingAnchor),
                newValue!.heightAnchor.constraint(equalTo: bookCoverContainer.heightAnchor)
            ])
        }
    }
    
    private let authorLabel: UILabel = {
        let label = UILabel()
        label.textColor = .label
        label.font = UIFont.preferredFont(forTextStyle: .body).bold
        label.textAlignment = .left
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let uploadLabel: UILabel = {
        let label = UILabel()
        label.textColor = .label
        label.font = UIFont.preferredFont(forTextStyle: .body).bold
        label.textAlignment = .left
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let priceLabel: UILabel = {
        let label = UILabel()
        label.textColor = .secondaryLabel
        label.font = UIFont.preferredFont(forTextStyle: .subheadline).bold
        label.textAlignment = .left
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let inCartCountLabel: UILabel = {
        let label = UILabel()
        label.textColor = .secondaryLabel
        label.font = UIFont.preferredFont(forTextStyle: .subheadline).bold
        label.textAlignment = .left
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let labelsStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .label
        label.font = UIFont.preferredFont(forTextStyle: .title1).bold
        label.textAlignment = .left
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let descriptionView: UITextView = {
        let textView = UITextView()
        textView.font = .preferredFont(forTextStyle: .body)
        textView.isEditable = false
        textView.isUserInteractionEnabled = true
        textView.isScrollEnabled = true
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()

    private let plusButton = Button(withTitle: "+")
    private let minusButton = Button(withTitle: "-")
    
    private let loadingView = LoadingView()
        
    private let takeButton = Button()
    
    private var disposeBag = DisposeBag()
    
    var viewModel: BookDetailsViewModel? {
        didSet {
            loadViewIfNeeded()
            bindViewModel()
        }
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupLayout()
    }
}

// MARK: - Private Extension

private extension BookDetailsVC {
    
    func setupLayout() {
        navigationItem.rightBarButtonItem = shareButton
        view.backgroundColor = .systemBackground
        
        view.addSubview(bookCoverContainer)
        view.addSubview(authorLabel)
        view.addSubview(uploadLabel)
        view.addSubview(labelsStack)
        labelsStack.addArrangedSubview(priceLabel)
        labelsStack.addArrangedSubview(inCartCountLabel)
        view.addSubview(titleLabel)
        view.addSubview(descriptionView)
        view.addSubview(plusButton)
        view.addSubview(minusButton)
        view.addSubview(takeButton)
        
        NSLayoutConstraint.activate([
            bookCoverContainer.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            bookCoverContainer.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 10),
            bookCoverContainer.trailingAnchor.constraint(equalTo: view.centerXAnchor, constant: -10),
            bookCoverContainer.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.width/1.5),
            
            authorLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 25),
            authorLabel.leadingAnchor.constraint(equalTo: view.centerXAnchor, constant: 10),
            authorLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10),
            
            uploadLabel.topAnchor.constraint(equalTo: authorLabel.bottomAnchor, constant: 5),
            uploadLabel.leadingAnchor.constraint(equalTo: view.centerXAnchor, constant: 10),
            uploadLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10),
            
            labelsStack.bottomAnchor.constraint(equalTo: bookCoverContainer.bottomAnchor, constant: -5),
            labelsStack.leadingAnchor.constraint(equalTo: view.centerXAnchor, constant: 10),
            labelsStack.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10),
            
            titleLabel.topAnchor.constraint(equalTo: bookCoverContainer.bottomAnchor, constant: 20),
            titleLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 10),
            titleLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10),

            descriptionView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            descriptionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 10),
            descriptionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10),
            descriptionView.bottomAnchor.constraint(equalTo: plusButton.topAnchor, constant: -10),
            
            takeButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -10),
            takeButton.heightAnchor.constraint(equalToConstant: takeButton.defaultHeight),
            takeButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            takeButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),

            minusButton.bottomAnchor.constraint(equalTo: takeButton.topAnchor, constant: -20),
            minusButton.heightAnchor.constraint(equalToConstant: minusButton.defaultHeight),
            minusButton.widthAnchor.constraint(equalToConstant: minusButton.defaultWidth / 2 - 10),
            minusButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20),

            plusButton.bottomAnchor.constraint(equalTo: takeButton.topAnchor, constant: -20),
            plusButton.heightAnchor.constraint(equalToConstant: plusButton.defaultHeight),
            plusButton.widthAnchor.constraint(equalToConstant: plusButton.defaultWidth / 2 - 10),
            plusButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20)
        ])
        
        view.addSubview(loadingView)
    }
    
    func bindViewModel() {
        disposeBag = .init()
        
        let viewWillAppear = rx
            .sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .asDriver(onErrorDriveWith: .never())
            .mapToVoid()
        
        guard let output = viewModel?.transform(.init(
            refreshTrigger: viewWillAppear,
            shareButton: shareButton.rx.tap.asDriver(),
            takeButton: takeButton.rx.tap.asDriver(),
            plusButton: plusButton.rx.tap.asDriver(),
            minusButton: minusButton.rx.tap.asDriver()
        )) else { return }
        
        disposeBag.insert(
            output.author.drive(authorLabel.rx.text),
            output.uploadedAt.drive(uploadLabel.rx.text),
            output.price.drive(priceLabel.rx.text),
            output.description.drive(descriptionView.rx.text),
            output.inCartCount.drive(inCartCountLabel.rx.text),
            output.cover.drive(onNext: { [unowned self] in
                bookCover = $0
            }),
            output.name.drive(titleLabel.rx.text),
            output.buttonTitle.drive(onNext: { [unowned self] in
                takeButton.title = $0
            }),
            output.isLoading.drive(onNext: { [unowned self] in
                loadingView.isPresented = $0
            }),
            output.isButtonEnabled.drive(onNext: { [unowned self] in
                takeButton.isActive = $0
            }),
            output.isStepperHidden.drive(onNext: { [unowned self] in
                plusButton.isHidden = $0
                minusButton.isHidden = $0
            }),
            output.triggers.drive()
        )
    }
}
