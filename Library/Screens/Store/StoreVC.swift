//
//  LibraryVC.swift
//  Library
//
//  Created by Yan Smaliak on 20.11.2020.
//

import UIKit
import RxSwift
import RxCocoa

final class LibraryVC: UIViewController {
    
    // MARK: - Properties
    
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.estimatedSectionHeaderHeight = 88
        tableView.sectionHeaderHeight = 0
        tableView.estimatedRowHeight = 88
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        return tableView
    }()
    
    private let spinner: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView()
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.style = .large
        return spinner
    }()
    
    private var disposeBag = DisposeBag()
    
    var viewModel: LibraryViewModelType? {
        didSet {
            loadViewIfNeeded()
            bindViewModel()
        }
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        setupLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !view.subviews.contains(tableView) {
            embedTableView()
        }
        if !view.subviews.contains(spinner) {
            embedSpinner()
        }
    }
}

// MARK: - TableView Delegate

extension LibraryVC: UITableViewDelegate { }

// MARK: - UIAdaptivePresentationControllerDelegate

extension LibraryVC: UIAdaptivePresentationControllerDelegate {
    func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {}
}

// MARK: - Private Extension

private extension LibraryVC {
    func setupLayout() {
        navigationItem.title = "Store"
        navigationController?.navigationBar.prefersLargeTitles = true
        view.backgroundColor = .systemBackground
    }
    
    func embedSpinner() {
        view.addSubview(spinner)
        
        NSLayoutConstraint.activate([
            spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }
    
    func embedTableView() {
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
        ])
    }
    
    func setupTableView() {
        tableView.delegate = self
        tableView.register(cellType: BookCell.self)
    }
    
    func bindViewModel() {
        disposeBag = .init()
        
        let viewWillAppear = rx
            .sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .asDriver(onErrorDriveWith: .never())
            .mapToVoid()
        
        let modalDismissed = rx
            .sentMessage(#selector(UIAdaptivePresentationControllerDelegate.presentationControllerDidDismiss(_:)))
            .asDriver(onErrorDriveWith: .never())
            .mapToVoid()
        
        guard let output = viewModel?.transform(.init(
            refreshTrigger: .merge(viewWillAppear, modalDismissed),
            selectedCell: tableView.rx.modelSelected(BookCellModel.self).asDriver()
        )) else { return }
        
        disposeBag.insert(
            output.cellsModels.drive(tableView.rx.items(cellIdentifier: BookCell.reuseIdentifier,
                                                            cellType: BookCell.self)) { (_, cellViewModel, cell) in
                cell.viewModel = cellViewModel
            },
            output.isLoading.drive(spinner.rx.isAnimating),
            output.triggers.drive()
        )
    }
}
