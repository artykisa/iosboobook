//
//  LibraryViewModel.swift
//  Library
//
//  Created by Yan Smaliak on 20.11.2020.
//

import Foundation
import RxSwift
import RxCocoa
import RxSwiftUtilities

// MARK: - Input

struct LibraryViewModelInput {
    let refreshTrigger: Driver<Void>
    let selectedCell: Driver<BookCellModel>
}

// MARK: - Output

struct LibraryViewModelOutput {
    let cellsModels: Driver<[BookCellModel]>
    let isLoading: Driver<Bool>
    let triggers: Driver<Void>
}

// MARK: - Books Show Mode Type

enum BooksShowModeType {
    case all
    case myBooks
}

// MARK: - Router Type

protocol LibraryRouterType: BaseRouterType {
    func showAddBookAlert() -> Single<BookName>
    func showBookModeSelectionMenu() -> Single<BooksShowModeType>
    func showDetails(forBookWithID bookID: BookID)
}

// MARK: - Router

final class LibraryRouter: BaseRouter, LibraryRouterType {
    
    func showAddBookAlert() -> Single<BookName> {
        .create { listener in
            let alert = UIAlertController(title: L10n.Localizable.addBook,
                                          message: nil,
                                          preferredStyle: .alert)
            
            let saveAction = UIAlertAction(title: L10n.Localizable.save, style: .default) { _ in
                let bookNameField = alert.textFields![0]
                let bookName = BookName(name: bookNameField.text ?? "")
                listener(.success(bookName))
            }
            
            let cancelAction = UIAlertAction(title: L10n.Localizable.cancel,
                                             style: .cancel)
            
            alert.addTextField {
                $0.placeholder = L10n.Localizable.bookName
                $0.autocapitalizationType = .sentences
            }
            
            alert.addAction(saveAction)
            alert.addAction(cancelAction)
            
            self.rootViewController?.present(alert, animated: true, completion: nil)
            
            return Disposables.create()
        }
    }
    
    func showBookModeSelectionMenu() -> Single<BooksShowModeType> {
        .create { listener in
            let showMenu = UIAlertController(title: nil,
                                             message: L10n.Localizable.chooseWhatYouWantToSee,
                                             preferredStyle: .actionSheet)
            
            let allBooksAction = UIAlertAction(title: L10n.Localizable.all.capitalized, style: .default) { _ in
                listener(.success(.all))
            }
            let myBooksAction = UIAlertAction(title: L10n.Localizable.myBooks.capitalized, style: .default) { _ in
                listener(.success(.myBooks))
            }
            let cancelAction = UIAlertAction(title: L10n.Localizable.cancel, style: .cancel)
            
            showMenu.addAction(allBooksAction)
            showMenu.addAction(myBooksAction)
            showMenu.addAction(cancelAction)
            
            self.rootViewController?.present(showMenu, animated: true)
            
            return Disposables.create()
        }
    }
    
    func showDetails(forBookWithID bookID: BookID) {
        let bookDetailsVC = BookDetailsVC()
        let bookDetailsNavigationController = UINavigationController(rootViewController: bookDetailsVC)
        let bookDetailsRouter = BookDetailsRouter(rootViewController: bookDetailsVC)
        let bookDetailsViewModel = BookDetailsViewModel(router: bookDetailsRouter,
                                                        repository: SwinjectContainer.container.resolve(BookRepository.self)!,
                                                        bookID: bookID)
        bookDetailsVC.viewModel = bookDetailsViewModel
        bookDetailsNavigationController.presentationController?.delegate = rootViewController as? UIAdaptivePresentationControllerDelegate
        rootViewController?.present(bookDetailsNavigationController, animated: true)
    }
}

protocol LibraryRepositoryType {
    func allBooks() -> Single<[Book]>
    func allBooksFromStorage() -> Single<[Book]>
    func addBook(withName: BookName) -> Single<Book>
}

// MARK: - View Model Type

protocol LibraryViewModelType {
    func transform(_ input: LibraryViewModelInput) -> LibraryViewModelOutput
}

// MARK: - View Model

struct LibraryViewModel: LibraryViewModelType {
    // MARK: - Properties
    
    private let router: LibraryRouterType
    private let repository: LibraryRepositoryType
    
    // MARK: - Init
    
    init(router: LibraryRouterType, repository: LibraryRepositoryType) {
        self.router = router
        self.repository = repository
    }
    
    // MARK: - Public Methods
    
    func transform(_ input: LibraryViewModelInput) -> LibraryViewModelOutput {
        let loadingSubject = ActivityIndicator()
        
        let loadBooksTrigger = input.refreshTrigger
        
        let booksFromStorage = loadBooksTrigger
            .flatMapLatest {
                repository
                    .allBooksFromStorage()
                    .trackActivity(loadingSubject)
                    .asDriver(onErrorDriveWith: .never())
            }
        
        let booksFromApi = loadBooksTrigger
            .flatMapLatest {
                repository
                    .allBooks()
                    .trackActivity(loadingSubject)
                    .asDriver(onErrorDo: router.showError)
            }
        
		let booksDriver: Driver<[Book]> = Driver
            .merge(booksFromApi, booksFromStorage)
        
        let booksToShow = booksDriver
        
        let cellsViewModels = booksToShow
            .map { $0.map { book -> BookCellModel in
                BookCellModel(book: book)
            }
            }
        
        let showBookDetails = input.selectedCell
            .map { router.showDetails(forBookWithID: $0.bookID) }
        
        let loadingSubjectDriver = loadingSubject
            .asDriver(onErrorJustReturn: false)
        
        let booksArrayIsEmptyDriver = booksToShow
            .map { $0.isEmpty }
        
        let isLoading = Driver
            .combineLatest(loadingSubjectDriver, booksArrayIsEmptyDriver, resultSelector: { ($0, $1) })
            .map { $0 && $1}
        
        return .init(
			cellsModels: cellsViewModels,
			isLoading: isLoading,
			triggers: showBookDetails
		)
    }
    
    // MARK: - Private Methods
    
    /// Generate string containing total number of book and number of user's books
    /// - Parameter array: array of ALL book
    /// - Returns: string with number of books
    private func booksCountString(inArray array: [Book]) -> String {
        let numberOfBooks = array.count
        let numberOfMyBooks = array.filter({ $0.isOwnedByCurrentUser }).count
        
        return "\(numberOfBooks) \(L10n.Localizable.total), " +
            "\(numberOfMyBooks) \(L10n.Localizable.ofMy)"
    }
}
