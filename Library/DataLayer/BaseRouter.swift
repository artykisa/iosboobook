//
//  BaseRouter.swift
//  Library
//
//  Created by Yan Smaliak on 26.01.2021.
//

import Foundation
import SPAlert

protocol BaseRouterType {
    func showError(_ error: Error)
}

class BaseRouter: BaseRouterType {
    
    // MARK: - Properties
    
    weak var rootViewController: UIViewController?
    private let decoder = JSONDecoder()
    
    // MARK: - Init
    
    init(rootViewController: UIViewController) {
        self.rootViewController = rootViewController
    }
    
    // MARK: - Public Methods
    
    func showAlert(withMessage message: String, andImage image: UIImage) {
        SPAlert.present(title: message, preset: .custom(image))
    }
    
    func showError(_ error: Error) {
        if let backendError = error as? BackendError {
            if let result = try? self.decoder.decode(Response<String>.self, from: backendError.payload) {
                showError(withMessage: result.data)
                return
            }
            
            if let result = try? self.decoder.decode(Response<SignupBadRequest>.self, from: backendError.payload) {
                showError(withMessage: result.data.errorMessage)
                return
            }
        }
        
        if error is ResponseError {
            showError(withMessage: "Ooops! Something went wrong!")
            return
        }
        
        if (error as NSError).domain == "NSURLErrorDomain" {
            showInternetConnectionError()
            return
        }
        
        if type(of: error) == DecodingError.self { return }
        
        showError(withMessage: "Ooops! Something went wrong!")
    }
    
    // MARK: - Private Methods
    
    private func showError(withMessage message: String) {
        showAlert(withMessage: message, andImage: UIImage(systemName: "xmark.octagon") ?? .remove)
    }
    
    private func showInternetConnectionError() {
        SPAlert.present(message: "Сheck your internet connection", haptic: .none)
    }
}
