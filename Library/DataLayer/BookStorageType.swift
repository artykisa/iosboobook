//
//  BookStorageType.swift
//  Library
//
//  Created by Yan Smaliak on 20.01.2021.
//

import Foundation

protocol BookStorageType {
    func allBooks(completion: @escaping (Result<[PayloadBook], Error>) -> Void)
    func bookBeingRead(by userID: UserID, completion: @escaping (Result<PayloadBook?, Error>) -> Void)
    func bookByID(bookID: BookID, completion: @escaping (Result<PayloadBook, Error>) -> Void)
	func cdBookByID(bookID: BookID) -> Result<CDBook, Error>
    func book(byId: BookID) -> Result<Book, Error>
    func save(book: PayloadBook, completion: @escaping (Result<Void, Error>) -> Void)
    func updateBooksIfNeeded(books: [PayloadBook], completion: @escaping (Result<Void, Error>) -> Void)
}
