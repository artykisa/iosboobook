//
//  KeychainCredentialsStore.swift
//  Library
//
//  Created by Yan Smaliak on 10.12.2020.
//

import Foundation
import SwiftKeychainWrapper

protocol CredentialsStoreType: class {
    var userCredentials: UserCredentials? { get set }
}

final class KeychainCredentialsStore: CredentialsStoreType {
    private let keychain: KeychainWrapper
    
    init(keychain: KeychainWrapper) {
        self.keychain = keychain
    }
    
    var userCredentials: UserCredentials? {
        get {
            guard let userID = keychain.integer(forKey: .userID) else { return nil }
            guard let userEmail = keychain.string(forKey: .userEmail) else { return nil }
            guard let token = keychain.string(forKey: .token) else { return nil }
            
            return UserCredentials(id: userID, email: userEmail, token: token)
        }
        set {
            guard let credentials = newValue else {
                keychain.removeAllKeys()
                return
            }
            
            keychain.set(credentials.id, forKey: .userID)
            keychain.set(credentials.email, forKey: .userEmail)
            keychain.set(credentials.token, forKey: .token)
        }
    }
}

private extension String {
    static let userID = "userID"
    static let userEmail = "userEmail"
    static let token = "token"
}
