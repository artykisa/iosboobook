//
//  CoreDataBookStorage.swift
//  Library
//
//  Created by Yan Smaliak on 12.01.2021.
//

import Foundation
import CoreData

final class CoreDataBookStorage: BookStorageType {
    // MARK: - Properties
    
    private let managedContext: NSManagedObjectContext
    
    // MARK: - Init
    
    init(managedContext: NSManagedObjectContext) {
        self.managedContext = managedContext
    }
    
    // MARK: - Public Methods
    
    func allBooks(completion: @escaping (Result<[PayloadBook], Error>) -> Void) {
        managedContext.perform {
            let booksFetch = CDBook.createFetchRequest()
            
            do {
                let fetchedBooks = try self.managedContext.fetch(booksFetch)
                let books = fetchedBooks.map { $0.book }
                completion(.success(books))
            } catch {
                print("Failed to fetch books: \(error)")
                completion(.failure(error))
            }
        }
    }
    
    func bookBeingRead(by userID: UserID, completion: @escaping (Result<PayloadBook?, Error>) -> Void) {
        managedContext.perform {
            let fetchRequest = CDBook.createFetchRequest()
//            fetchRequest.predicate = NSPredicate(format: "readerUserID == %@", "\(userID)")
            
            do {
                let fetchedBooks = try self.managedContext.fetch(fetchRequest)
                if fetchedBooks.isEmpty {
                    completion(.success(nil))
                } else {
                    completion(.success(fetchedBooks[0].book))
                }
            } catch {
                print("Failed to get book that being read by user: \(error)")
                completion(.failure(error))
            }
        }
    }
    
    func bookByID(bookID: BookID, completion: @escaping (Result<PayloadBook, Error>) -> Void) {
        managedContext.perform {
            let fetchRequest = CDBook.createFetchRequest()
            fetchRequest.predicate = NSPredicate(format: "id == %@", "\(bookID)")
            
            do {
                let fetchedBooks = try self.managedContext.fetch(fetchRequest)
                if fetchedBooks.isEmpty {
                    completion(.failure(CoreDataError.bookIsNotInStorage))
                } else {
                    completion(.success(fetchedBooks[0].book))
                }
            } catch {
                print("Failed to load book from storage: \(error)")
                completion(.failure(error))
            }
        }
    }

    func book(byId bookID: BookID) -> Result<Book, Error> {
        let fetchRequest = CDBook.createFetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id == %@", "\(bookID)")

        do {
            let fetchedBooks = try self.managedContext.fetch(fetchRequest)
            if fetchedBooks.isEmpty {
                return .failure(CoreDataError.bookIsNotInStorage)
            } else {
                return .success(Book(payload: fetchedBooks[0].book,
                                     isBeingReadByCurrentUser: false,
                                     isOwnedByCurrentUser: false))
            }
        } catch {
            print("Failed to load book from storage: \(error)")
            return .failure(error)
        }
    }
	
	func cdBookByID(bookID: BookID) -> Result<CDBook, Error> {
		let fetchRequest = CDBook.createFetchRequest()
		fetchRequest.predicate = NSPredicate(format: "id == %@", "\(bookID)")
		
		do {
			let fetchedBooks = try self.managedContext.fetch(fetchRequest)
			if fetchedBooks.isEmpty {
				return .failure(CoreDataError.bookIsNotInStorage)
			} else {
				return .success(fetchedBooks[0])
			}
		} catch {
			print("Failed to load book from storage: \(error)")
			return .failure(error)
		}
	}
    
    func save(book: PayloadBook, completion: @escaping (Result<Void, Error>) -> Void) {
        managedContext.perform {
            let fetchRequest = CDBook.createFetchRequest()
            fetchRequest.predicate = NSPredicate(format: "id == %@", "\(book.id)")
            
            do {
                let fetchedBooks = try self.managedContext.fetch(fetchRequest)
                var currentBook: CDBook
                
                if fetchedBooks.isEmpty {
                    currentBook = CDBook(context: self.managedContext)
                } else {
                    currentBook = fetchedBooks[0]
                    if book.updatedAt == currentBook.updatedAt { return }
                }
                
                currentBook.id = Int32(book.id)
                currentBook.name = book.name
                currentBook.createdAt = book.createdAt
                currentBook.updatedAt = book.updatedAt
                currentBook.price = book.price
                currentBook.bookDescription = book.description
                currentBook.author = book.author
                currentBook.image = book.image
                currentBook.count = Int16(book.count)
                
                try currentBook.managedObjectContext?.save()
                completion(.success(()))
            } catch {
                print("Failed to save book: \(error)")
                completion(.failure(error))
            }
        }
    }

    func updateCount(for book: PayloadBook, newValue: Int) {

    }
    
    func updateBooksIfNeeded(books: [PayloadBook], completion: @escaping (Result<Void, Error>) -> Void) {
        for book in books {
            save(book: book, completion: {
                do {
                    try $0.get()
                } catch {
                    completion(.failure(error))
                }
            })
        }
        booksCount(completion: {
            do {
                let count = try $0.get()
                if count > books.count {
                    self.allBooks(completion: {
                        do {
                            let booksFromStorage = try $0.get()
                            let booksToDelete = booksFromStorage.filter { book in
                                !books
                                    .map { $0.id }
                                    .contains(book.id)
                            }
                            
                            for book in booksToDelete {
                                self.delete(bookWithID: book.id, completion: {
                                    do {
                                        try $0.get()
                                    } catch {
                                        completion(.failure(error))
                                    }
                                })
                            }
                        } catch {
                            completion(.failure(error))
                        }
                    })
                }
            } catch {
                completion(.failure(error))
            }
        })
        completion(.success(()))
    }
    
    // MARK: - Private Methods
    
    private func delete(bookWithID bookID: BookID, completion: @escaping (Result<Void, Error>) -> Void) {
        managedContext.perform {
            let fetchRequest = CDBook.createFetchRequest()
            fetchRequest.predicate = NSPredicate(format: "id == %@", "\(bookID)")
            
            do {
                let fetchedBooks = try self.managedContext.fetch(fetchRequest)
                
                if !fetchedBooks.isEmpty {
                    self.managedContext.delete(fetchedBooks[0])
                    try self.managedContext.save()
                    completion(.success(()))
                }
            } catch {
                print("Failed to save book: \(error)")
                completion(.failure(error))
            }
        }
    }
    
    private func booksCount(completion: @escaping (Result<Int, Error>) -> Void) {
        managedContext.perform {
            let booksFetch = CDBook.createFetchRequest()
            
            do {
                let fetchedBooks = try self.managedContext.fetch(booksFetch)
                completion(.success(fetchedBooks.count))
            } catch {
                print("Failed to fetch books: \(error)")
                completion(.failure(error))
            }
        }
    }
}
