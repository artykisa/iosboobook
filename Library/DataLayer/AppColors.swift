//
//  AppColors.swift
//  Library
//
//  Created by Yan Smaliak on 01.12.2020.
//

import UIKit

final class AppColors {
    static let coverColors: [UIColor] = [.systemBlue, .systemGreen, .systemIndigo, .systemOrange, .systemPink, .systemPurple, .systemRed]
    static let shadow: UIColor = UIColor(dynamicProvider: {
        switch $0.userInterfaceStyle {
        case .light, .unspecified:
            return .systemGray
        case .dark:
            return .black
        @unknown default:
            assertionFailure("Unknown userInterfaceStyle: \($0.userInterfaceStyle)")
            return .systemGray
        }
    })
    
    static func coverColor(for book: PayloadBook) -> UIColor {
        coverColors[abs(book.name.hash) % coverColors.count]
    }
}
