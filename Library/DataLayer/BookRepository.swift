//
//  BookRepository.swift
//  Library
//
//  Created by Yan Smaliak on 12.01.2021.
//

import Foundation
import RxSwift

final class BookRepository: LibraryRepositoryType, BookDetailsRepositoryType, AccountRepositoryType, CartRepositoryType {
	
    // MARK: - Properties
    
    private let apiController: ApiControllerType
    private let bookStorage: BookStorageType
    private let currentUserID: UserID
    private let defaults = UserDefaults.standard
    
    // MARK: - Init
    
	init(apiController: ApiControllerType, bookStorage: BookStorageType, currentUserID: UserID) {
        self.apiController = apiController
        self.bookStorage = bookStorage
        self.currentUserID = currentUserID
    }
    
    // MARK: - Public Methods
    
    func allBooks() -> Single<[Book]> {
        apiController
            .call(.allBooks())
			.do(onError: { print($0) })
            .do(onSuccess: {
                self.bookStorage
                    .updateBooksIfNeeded(books: $0, completion: {
                        do {
                            try $0.get()
                        } catch {
                            print(error)
                        }
                    })
            })
            .map { books in
                books.map(self.mapToBook)
            }
    }
    
    func allBooksFromStorage() -> Single<[Book]> {
        .create { single in
            let disposable = Disposables.create()
            
            self.bookStorage
                .allBooks(completion: { payloadBooks in
                    do {
                        let books = try payloadBooks.get()
                            .map { self.mapToBook(payloadBook: $0) }
                        single(.success(books))
                    } catch {
                        single(.error(error))
                    }
                    
                })
            
            return disposable
        }
    }
    
    func addBook(withName bookName: BookName) -> Single<Book> {
        apiController
            .call(.addBook(withName: bookName))
            .do(onSuccess: {
                self.bookStorage
                    .save(book: $0, completion: {
                        do {
                            try $0.get()
                        } catch {
                            print(error)
                        }
                    })
            })
            .map(mapToBook)
    }
    
    func loadBook(withID bookID: BookID) -> Single<Book> {
        apiController
            .call(.bookByID(bookID: bookID))
            .do(onSuccess: {
                self.bookStorage
                    .save(book: $0, completion: {
                        do {
                            try $0.get()
                        } catch {
                            print(error)
                        }
                    })
            })
            .map(mapToBook)
    }
    
    func loadBookFromStorage(withID bookID: BookID) -> Single<Book> {
        .create { single in
            let disposable = Disposables.create()
            
            self.bookStorage
                .bookByID(bookID: bookID, completion: {
                    do {
                        let payloadBook = try $0.get()
                        single(.success(self.mapToBook(payloadBook: payloadBook)))
                    } catch {
                        single(.error(error))
                    }
                })
            
            return disposable
        }
    }
    
    func takeOrReturnBookAction(forBookWithID bookID: BookID, andReaderUserID readerUserID: UserID?) -> Single<Book> {
        apiController
            .call(readerUserID == self.currentUserID ? .returnBook(withId: bookID) : .reserveBook(withId: bookID))
            .do(onSuccess: {
                self.bookStorage
                    .save(book: $0, completion: {
                        do {
                            try $0.get()
                        } catch {
                            print(error)
                        }
                    })
            })
            .map(mapToBook)
    }
	
	func addOrRemove(bookWithId bookId: BookID) -> Single<Book> {
        let book = try! bookStorage.book(byId: bookId).get()
        let cart = defaults.object(forKey: "Cart") as? [Int] ?? []

        if cart.contains(where: { $0 == bookId }) {
            return apiController
                .call(.removeAllFromCart(bookWithId: book.payload.id))
                .do(onSuccess: { cartPayload in
                    self.syncCart(payload: cartPayload)
                })
                .map { _ in book }
        } else {
            return apiController
                .call(.addToCart(bookWithId: book.payload.id))
                .do(onSuccess: { cartPayload in
                    self.syncCart(payload: cartPayload)
                })
                .map { _ in book }
        }
	}

    func add(bookWithId bookId: BookID) -> Single<Book> {
        let book = try! bookStorage.book(byId: bookId).get()

        return apiController
            .call(.addToCart(bookWithId: book.payload.id))
            .do(onSuccess: { cartPayload in
                self.syncCart(payload: cartPayload)
            })
            .map { _ in book }
    }

    func remove(bookWithId bookId: BookID) -> Single<Book> {
        let book = try! bookStorage.book(byId: bookId).get()

        return apiController
            .call(.removeFromCart(bookWithId: book.payload.id))
            .do(onSuccess: { cartPayload in
                self.syncCart(payload: cartPayload)
            })
            .map { _ in book }
    }
	
	func isCartContains(bookWithId bookId: BookID) -> Bool {
        let cart = defaults.object(forKey: "Cart") as? [Int] ?? []
        return cart.contains(bookId)
	}
    
    func bookBeingReadByCurrentUserFromStorage() -> Single<Book?> {
        .create { single in
            let disposable = Disposables.create()
            
            self.bookStorage
                .bookBeingRead(by: self.currentUserID, completion: {
                    do {
                        let payloadBook = try $0.get()
                        payloadBook == nil ? single(.success(nil)) : single(.success(self.mapToBook(payloadBook: payloadBook!)))
                    } catch {
                        single(.error(error))
                    }
                })
            
            return disposable
        }
    }

    func order(_ order: Order) -> Single<[Book]> {
        apiController
            .call(.order(order))
            .do(onError: { print($0) })
            .do(onSuccess: { cartPayload in
                var cart = self.defaults.object(forKey: "Cart") as? [Int] ?? []
                for book in cartPayload.books {
                    if !cart.contains(book.id) {
                        cart.append(book.id)
                    }
                }
                self.defaults.set(cart, forKey: "Cart")
            })
            .map { cartPayload in
                cartPayload.books.map(self.mapToBook)
            }
    }
	
	func booksInCart() -> Single<[Book]> {
        apiController
            .call(.cart())
            .do(onError: { print($0) })
            .do(onSuccess: { cartPayload in
                var cart = self.defaults.object(forKey: "Cart") as? [Int] ?? []
                for book in cartPayload.books {
                    if !cart.contains(book.id) {
                        cart.append(book.id)
                    }
                }
                self.defaults.set(cart, forKey: "Cart")
            })
            .map { cartPayload in
                cartPayload.books.map(self.mapToBook)
            }
	}
	
	func booksInCartFromStorage() -> Single<[Book]> {
        .create { single in
            let disposable = Disposables.create()

            var books: [Book] = []
            let cart = self.defaults.object(forKey: "Cart") as? [Int] ?? []

            for bookId in cart {
                let book = try! self.bookStorage.book(byId: bookId).get()
                books.append(book)
            }

            single(.success(books))

            return disposable
        }
	}

    func bonuses() -> Single<Double> {
        apiController
            .call(.bonuses())
            .do(onError: { print($0) })
            .do(onSuccess: { user in
                self.defaults.set(user.bonuses, forKey: "Bonuses")
            })
            .map { $0.bonuses }
    }
    
    // MARK: - Private Methods
    
    private func mapToBook(payloadBook: PayloadBook) -> Book {
        Book(payload: payloadBook,
             isBeingReadByCurrentUser: false,
             isOwnedByCurrentUser: false)
    }

    private func syncCart(payload: CartPayload) {
        var cart = defaults.object(forKey: "Cart") as? [Int] ?? []
        cart = []
        for book in payload.books {
            if !cart.contains(book.id) {
                cart.append(book.id)
            }
        }
        defaults.set(cart, forKey: "Cart")
    }
}
