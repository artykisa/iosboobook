//
//  BookName.swift
//  Library
//
//  Created by Yan Smaliak on 01.01.2021.
//

import Foundation

struct BookName: Encodable {
    let name: String
}
