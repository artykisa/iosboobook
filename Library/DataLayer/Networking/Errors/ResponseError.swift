//
//  ResponseError.swift
//  Library
//
//  Created by Yan Smaliak on 15.11.2020.
//

import Foundation

enum ResponseError: Error {
    case badStatusCode
}
