//
//  HTTPMethod.swift
//  Library
//
//  Created by Yan Smaliak on 11/12/20.
//

import Foundation

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}
