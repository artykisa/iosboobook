//
//  SignupBadRequest.swift
//  Library
//
//  Created by Yan Smaliak on 22.01.2021.
//

import Foundation

/// Error structure for signup endpoint
struct SignupBadRequest: Decodable {
    let mail: [String]?
    let password: [String]?
    
    var errorMessage: String {
        var errorMessage = ""
        
        if let emailErrors = mail {
            for error in emailErrors {
                errorMessage += "Email \(error).\n"
            }
        }
        
        if let passwordErrors = password {
            for error in passwordErrors {
                errorMessage += "Password \(error).\n"
            }
        }
        
        if errorMessage.count >= 2 {
            errorMessage = String(errorMessage.dropLast(2))
        }
        
        return errorMessage
    }
}
