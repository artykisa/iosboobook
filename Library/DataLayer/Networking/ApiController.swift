//
//  ApiController.swift
//  Library
//
//  Created by Yan Smaliak on 11/12/20.
//

import Foundation
import RxSwift

protocol ApiControllerType {
    func call<ResponseType: Decodable>(_ request: Request<ResponseType>) -> Single<ResponseType>
	func newCall<ResponseType: Decodable>(_ request: Request<ResponseType>) -> Single<ResponseType>
}

final class ApiController: ApiControllerType {
    
    // MARK: - Properties
    
    private let baseURL = URL(string: "https://ake.qulix.com/api")!
    private let encoder = JSONEncoder()
    private let decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .bookDates
        return decoder
    }()
    private let credentialsStore: CredentialsStoreType
    
    // MARK: - Init
    
    init(credentialsStore: CredentialsStoreType) {
        self.credentialsStore = credentialsStore
    }
    
    // MARK: - Public Methods
    
    func call<ResponseType: Decodable>(_ request: Request<ResponseType>) -> Single<ResponseType> {
        return .create { single in
            let session = URLSession.shared
            let request = self.buildRequest(request)
            return session.rx.response(request: request)
                .asSingle()
                .subscribe(onSuccess: { response, data in
                    guard 200...299 ~= response.statusCode else {
                        if response.statusCode == 400 {                            
                            single(.error(BackendError(payload: data)))
                            return
                        }
                        
                        single(.error(ResponseError.badStatusCode))
                        return
                    }

                    do {
                        let result = try self.decoder.decode(ResponseType.self, from: data)
                        single(.success(result))
                    } catch {
                        single(.error(error))
                    }
                    return

                }, onError: { error in
                    single(.error(error))
                    return
                })
        }
    }
	
	func newCall<ResponseType: Decodable>(_ request: Request<ResponseType>) -> Single<ResponseType> {
		return .create { single in
			let session = URLSession.shared
			let request = self.buildRequest(request)
			return session.rx.response(request: request)
				.asSingle()
				.subscribe(onSuccess: { response, data in
					guard 200...299 ~= response.statusCode else {
						if response.statusCode == 400 {
							single(.error(BackendError(payload: data)))
							return
						}
						
						single(.error(ResponseError.badStatusCode))
						return
					}

					do {
						let result = try self.decoder.decode(ResponseType.self, from: data)
						single(.success(result))
					} catch {
						single(.error(error))
					}
					return

				}, onError: { error in
					single(.error(error))
					return
				})
		}
	}
    
    // MARK: - Private Methods
        
    private func buildRequest<ResponseType: Decodable>(_ request: Request<ResponseType>) -> URLRequest {
        let url = baseURL.appendingPathComponent(request.endpoint)
        
        var finalRequest = URLRequest(url: url)
        let urlComponents = NSURLComponents(url: url, resolvingAgainstBaseURL: true)!
        
        if !request.queryItems.isEmpty {
            var queryItems: [URLQueryItem] = []
            
            for item in request.queryItems {
                queryItems.append(URLQueryItem(name: item.0, value: item.1))
            }
            
            urlComponents.queryItems = queryItems
        }
        
        finalRequest.url = urlComponents.url!
        finalRequest.httpMethod = request.httpMethod.rawValue

        for header in request.headers {
            finalRequest.setValue(header.1, forHTTPHeaderField: header.0)
        }
        
        if request.isAuthorizationNeeded {
            if let token = credentialsStore.userCredentials?.token {
                finalRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            }
        }
        
        if let body = request.body {
            do {
                let data = try encoder.encode(body.asAnyEncodable)
                finalRequest.httpBody = data
            } catch {
                debugPrint(DataError.cannotEncode)
            }
        }
        
        return finalRequest
    }
}
