//
//  PayloadBook.swift
//  Library
//
//  Created by Yan Smaliak on 20.11.2020.
//

import Foundation

typealias BookID = Int

struct PayloadBook: Decodable {
    let id: BookID
    let name: String
    let createdAt: Date
    let updatedAt: Date
    let image: String?
    let price: Double
    let description: String?
    let author: String
    let count: Int

    init(id: BookID,
         name: String,
         createdAt: Date,
         updatedAt: Date,
         image: String?,
         price: Double,
         description: String?,
         author: String,
         count: Int
    ) {
        self.id = id
        self.name = name
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.image = image
        self.price = price
        self.description = description
        self.author = author
        self.count = count
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        createdAt = try container.decode(Date.self, forKey: .createdAt)
        updatedAt = try container.decode(Date.self, forKey: .updatedAt)
        image = try? container.decode(String?.self, forKey: .image)
        price = try container.decode(Double.self, forKey: .price)
        description = try? container.decode(String.self, forKey: .description)
        let authorStruct = try container.decode(Author.self, forKey: .author)
        author = authorStruct.name
        let optionalCount = try? container.decode(Int.self, forKey: .count)
        count = optionalCount ?? 0
    }
	
	enum CodingKeys: String, CodingKey {
        case id
        case name
        case createdAt
        case updatedAt
        case deadLine
        case image
        case price
        case description
        case author
        case count
    }
}

struct Author: Decodable {
    let name: String

    enum CodingKeys: CodingKey {
        case name
    }
}
