//
//  TextField.swift
//  Library
//
//  Created by Yan Smaliak on 11/11/20.
//

import UIKit

final class TextField: UITextField {
    
    // MARK: - Public
    
    public let defaultHeight: CGFloat = 50
    public let defaultWidth: CGFloat = UIScreen.main.bounds.width - 40
    
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    // MARK: - Private
    
    private func commonInit() {
        font = UIFont.preferredFont(forTextStyle: .body)
        layer.cornerRadius = 15
        keyboardType = UIKeyboardType.default
        returnKeyType = .done
        clearButtonMode = UITextField.ViewMode.whileEditing
        textAlignment = .left
        backgroundColor = .secondarySystemBackground
        translatesAutoresizingMaskIntoConstraints = false
        leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: frame.height))
        leftViewMode = .always
    }
}
