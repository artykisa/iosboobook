//
//  ShowModeButton.swift
//  Library
//
//  Created by Yan Smaliak on 24.11.2020.
//

import Foundation
import UIKit

final class ShowModeButton: UIButton {
    
    // MARK: - Public
    
    public var title: String {
        get {
            return buttonTitleLabel.text ?? ""
        }
        set {
            buttonTitleLabel.text = newValue
        }
    }
    
    // MARK: - Private
    
    private let buttonTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .label
        label.font = UIFont.preferredFont(forTextStyle: .caption1).bold
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let chevronSymbol: UIImageView = {
        let imageView = UIImageView()
        let config = UIImage.SymbolConfiguration(textStyle: .caption1)
        imageView.image = UIImage(systemName: "chevron.down", withConfiguration: config)
        imageView.tintColor = .label
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        addSubview(buttonTitleLabel)
        addSubview(chevronSymbol)
        
        NSLayoutConstraint.activate([
            buttonTitleLabel.topAnchor.constraint(equalTo: topAnchor),
            buttonTitleLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            buttonTitleLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            
            chevronSymbol.centerYAnchor.constraint(equalTo: centerYAnchor),
            chevronSymbol.leadingAnchor.constraint(equalTo: buttonTitleLabel.trailingAnchor),
            chevronSymbol.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
        
        translatesAutoresizingMaskIntoConstraints = false
    }
}
