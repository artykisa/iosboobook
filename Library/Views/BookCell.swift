//
//  BookCell.swift
//  Library
//
//  Created by Yan Smaliak on 26.11.2020.
//

import UIKit

final class BookCell: UITableViewCell, Reusable {
    
    // MARK: - Public
    
    var viewModel: BookModelType? {
        didSet {
            title.text = viewModel?.title
            readingNowLabel.text = viewModel?.author
            bookStatusLabel.text = viewModel?.price
            myBookLabel.text = viewModel?.myBook
            cover = viewModel?.cover ?? UIView()
            
            commonInit()
        }
    }
    
    // MARK: - Private
    
    private var cover: UIView! {
        didSet { oldValue?.removeFromSuperview() }
    }
    
    private let title: UILabel = {
        let label = UILabel()
        label.textColor = .label
        label.numberOfLines = 3
        label.font = UIFont.preferredFont(forTextStyle: .headline).bold
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let readingNowLabel: UILabel = {
        let label = UILabel()
        label.textColor = .secondaryLabel
        label.font = UIFont.preferredFont(forTextStyle: .footnote).bold
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let myBookLabel: UILabel = {
        let label = UILabel()
        label.textColor = .secondaryLabel
        label.font = UIFont.preferredFont(forTextStyle: .footnote).bold
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let bookStatusLabel: UILabel = {
        let label = UILabel()
        label.textColor = .secondaryLabel
        label.font = UIFont.preferredFont(forTextStyle: .footnote).bold
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let labelsStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillProportionally
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    // MARK: - Init
    
    private func commonInit() {
        selectionStyle = .none
        
        addSubview(cover)
        addSubview(title)
        addSubview(labelsStack)
        if readingNowLabel.text != nil {
            labelsStack.addArrangedSubview(readingNowLabel)
        }
        if myBookLabel.text != nil {
            labelsStack.addArrangedSubview(myBookLabel)
        }
        labelsStack.addArrangedSubview(bookStatusLabel)
        
        let coverHeightConstraint = cover.heightAnchor.constraint(equalToConstant: 90)
        coverHeightConstraint.priority = .defaultHigh
        
        NSLayoutConstraint.activate([
            cover.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            cover.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            coverHeightConstraint,
            cover.widthAnchor.constraint(equalToConstant: 60),
            cover.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            
            title.topAnchor.constraint(equalTo: topAnchor, constant: 15),
            title.leadingAnchor.constraint(equalTo: cover.trailingAnchor, constant: 10),
            title.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            title.bottomAnchor.constraint(equalTo: labelsStack.topAnchor, constant: -10),
            
            labelsStack.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15),
            labelsStack.leadingAnchor.constraint(equalTo: cover.trailingAnchor, constant: 10),
            labelsStack.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10)
        ])
    }
}
