//
//  CartHeaderView.swift
//  Library
//
//  Created by Yan Smaliak on 20.11.2020.
//

import UIKit

final class CartHeaderView: UIView {
    
    // MARK: - Public

    public var booksCountLabelText: String {
        get {
            return booksCountLabel.text ?? ""
        }
        set {
            booksCountLabel.text = newValue
        }
    }
    
    // MARK: - Private

    private let booksCountLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .secondaryLabel
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        label.textAlignment = .left
        return label
    }()
    
    private let topSeparatorView = SeparatorView()

    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        backgroundColor = .systemBackground
        
        addSubview(topSeparatorView)
        addSubview(booksCountLabel)

        NSLayoutConstraint.activate([
            topSeparatorView.topAnchor.constraint(equalTo: topAnchor),
            topSeparatorView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            topSeparatorView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
            
            booksCountLabel.topAnchor.constraint(equalTo: topSeparatorView.bottomAnchor, constant: 12),
            booksCountLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15)
        ])
    }

}
