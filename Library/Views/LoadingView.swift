//
//  LoadingView.swift
//  Library
//
//  Created by Yan Smaliak on 07.01.2021.
//

import UIKit

final class LoadingView: UIView {
    
    // MARK: - Public Properties
    
    public var isPresented: Bool {
        get {
            isHidden
        }
        set {
            isHidden = !newValue
            newValue ? spinner.startAnimating() : spinner.stopAnimating()
        }
    }

    // MARK: - Private Properties
    
    private let blur: UIVisualEffectView = {
        let view = UIVisualEffectView(effect: UIBlurEffect(style: .systemThinMaterial))
        view.isUserInteractionEnabled = false
        view.layer.masksToBounds = true
        view.layer.cornerRadius = 8
        return view
    }()
    
    private let spinner: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView()
        spinner.style = .large
        return spinner
    }()
    
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        backgroundColor = .clear
        layer.cornerRadius = 8
        frame = CGRect(x: 0, y: 0, width: 250, height: 250) // it's 250 because I'm using SPAlert and its size is 250
        center = CGPoint(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height/2 - 50) // there is a problem with iPad,
                                                                                                 // for some reason it's not as iPad center calculated
                
        addSubview(blur)
        addSubview(spinner)
        
        layoutSubviews()
    }
    
    // MARK: - Internal Methods
    
    override func layoutSubviews() {
        super.layoutSubviews()
        blur.frame = bounds
        spinner.frame = bounds
    }
}
