//
//  Button.swift
//  Library
//
//  Created by Yan Smaliak on 11/11/20.
//

import UIKit

final class Button: UIButton {
    
    // MARK: - Public
    
    public let defaultHeight: CGFloat = 50
    public let defaultWidth: CGFloat = UIScreen.main.bounds.width - 40
    
    public var title: String {
        get {
            titleLabel?.attributedText?.string ?? ""
        }
        set {
            let font = UIFont.preferredFont(forTextStyle: .title3)
            let attributes = [NSAttributedString.Key.font: font]
            let attributedTitle = NSAttributedString(string: newValue, attributes: attributes)
            
            setAttributedTitle(attributedTitle, for: .normal)
            setTitleColor(.white, for: .normal)
        }
    }
    
    public var isActive: Bool {
        get {
            isEnabled
        }
        set {
            isEnabled = newValue
            layer.backgroundColor = newValue ? UIColor.black.cgColor : UIColor.darkGray.cgColor
        }
    }
    
    // MARK: - Init
    
    init(withTitle title: String) {
        super.init(frame: .zero)
        commonInit()
        self.title = title
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        layer.cornerRadius = 15
        layer.backgroundColor = UIColor.black.cgColor
        layer.borderWidth = 1
        layer.borderColor = UIColor.secondarySystemBackground.cgColor
        translatesAutoresizingMaskIntoConstraints = false
    }
}
