//
//  DateDecodingStrategy+Extension.swift
//  Library
//
//  Created by Yan Smaliak on 11.12.2020.
//

import Foundation

extension JSONDecoder.DateDecodingStrategy {
    static var bookDates: JSONDecoder.DateDecodingStrategy {
        .custom { decoder in
            let container = try decoder.singleValueContainer()
            let dateString = try container.decode(String.self)
            guard let date = DateFormatter.yyyyMMddTHHmmss.date(from: dateString) ?? DateFormatter.iso8601Full.date(from: dateString) else {
                throw DecodingError.dataCorruptedError(in: container, debugDescription: "Cannot decode date string \(dateString)")
            }
            return date
        }
    }
}
