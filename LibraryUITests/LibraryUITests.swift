//
//  LibraryUITests.swift
//  LibraryUITests
//
//  Created by Yan Smaliak on 31.01.2021.
//

import XCTest

final class LibraryUITests: XCTestCase {
    
    var app: XCUIApplication!
    
    override func setUp() {
        app = XCUIApplication()
        app.launch()
    }
    
    func testAddBook() {
        let addButton = app.navigationBars["Library"].buttons["Add"]
        addButton.tap()
        
        let elementsQuery = app.alerts["Add Book"].scrollViews.otherElements
        let bookNameTextField = elementsQuery.collectionViews.textFields["Book name"]
        
        XCTAssertTrue(bookNameTextField.exists)
        
        let cancelButton = elementsQuery.buttons["Cancel"]
        let saveButton = elementsQuery.buttons["Save"]
        
        XCTAssertTrue(cancelButton.exists)
        XCTAssertTrue(saveButton.exists)
    }
    
    func testAccountPage() {
        let accountTab = app.tabBars["Tab Bar"].buttons["Account"]
        accountTab.tap()
        
        let accountNavigationBar = app.navigationBars["Account"]
        let accountNavigationBarLabel = accountNavigationBar.staticTexts["Account"]
        
        XCTAssertTrue(accountNavigationBar.exists)
        XCTAssertTrue(accountNavigationBarLabel.exists)
        
        let logoutButton = accountNavigationBar.buttons["Log Out"]
        
        XCTAssertTrue(logoutButton.exists)
    }
    
    func testTableViewSwipe() {
        app.navigationBars["Library"].staticTexts["Library"].tap()
        let tableElement = app.tables
            .cells.containing(.staticText, identifier: "Test3545")
            .children(matching: .staticText)
            .matching(identifier: "Test3545")
            .element(boundBy: 1)
            
        XCTAssertTrue(tableElement.exists)
        XCTAssertNoThrow(tableElement.swipeUp())
    }
    
    func testBookDetails() {
        app.navigationBars["Library"].staticTexts["Library"].tap()
        let tableElement = app.tables
            .children(matching: .cell)
            .element(boundBy: 0)
            .children(matching: .staticText)
            .matching(identifier: "Test")
            .element(boundBy: 1)
            
        XCTAssertTrue(tableElement.exists)
        tableElement.tap()
                
        let uploadedLabel = app.staticTexts["Uploaded at 5.10.2020"]
        XCTAssertTrue(uploadedLabel.exists)
        
        let shareButton = app.navigationBars["Library.BookDetailsVC"].buttons["Share"]
        XCTAssertTrue(shareButton.exists)
        
        let reserveButton = app.buttons["Reserve"]
        let returnButton = app.buttons["Return"]
        if !reserveButton.exists && !returnButton.exists {
            XCTFail("One button should exist")
        }
    }
}
