//
//  AccountViewModelTests.swift
//  LibraryTests
//
//  Created by Yan Smaliak on 29.01.2021.
//

import XCTest
import RxSwift
import RxCocoa
import RxBlocking
@testable import Library

final class AccountViewModelTests: XCTestCase {
    
    func testTransform() {
        let router = AccountRouterTest()
        let repository = AccountRepositoryTest()
        let email = "email"
        
        let viewModel = AccountViewModel(
            router: router,
            repository: repository,
            currentUserEmail: email,
            logOut: { () }
        )
        
        let output = viewModel.transform(
            AccountViewModelInput(
                refreshTrigger: Driver.from(optional: ()),
                logoutButton: Driver.from(optional: ()),
                selectedCell: Driver.from(optional: BookCellModel(book: .empty))
            )
        )
        
        XCTAssertEqual(try output.cellModel.toBlocking(timeout: 3).last()!.count, 1)
        XCTAssertEqual(try output.cellModel.toBlocking(timeout: 3).last()!.first?.bookID, Book.empty.payload.id)
        XCTAssertEqual(try output.emailLabel.toBlocking(timeout: 3).last()!, email)
        XCTAssertFalse(try output.isLoading.toBlocking(timeout: 3).first()!)
    }
    
}

final class AccountRouterTest: BaseRouterTest, AccountRouterType {
    func showDetails(forBookWithID bookID: BookID) {
        debugPrint("Show details for book with id: \(bookID)")
    }
}

final class AccountRepositoryTest: AccountRepositoryType {
    func bookBeingReadByCurrentUser() -> Single<Book?> {
        .create { single in
            let disposable = Disposables.create()
            single(.success(.empty))
            return disposable
        }
    }
    
    func bookBeingReadByCurrentUserFromStorage() -> Single<Book?> {
        .create { single in
            let disposable = Disposables.create()
            single(.success(.empty))
            return disposable
        }
    }
}
