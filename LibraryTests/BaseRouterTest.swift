//
//  BaseRouterTest.swift
//  LibraryTests
//
//  Created by Yan Smaliak on 29.01.2021.
//

import Foundation
@testable import Library

class BaseRouterTest: BaseRouterType {
    func showError(_ error: Error) {
        debugPrint("Show error: \(error)")
    }
}
