//
//  LoginViewModelTests.swift
//  LibraryTests
//
//  Created by Yan Smaliak on 29.01.2021.
//

import XCTest
import RxSwift
import RxCocoa
import RxBlocking
@testable import Library

final class LoginViewModelTests: XCTestCase {
    
    func testTransform() {
        let router = LoginRouterTest()
        let apiController = ApiControllerTest()
        let credentialsStore = CredentialsStoreTest()
        
        let viewModel = LoginViewModel(
            router: router,
            apiController: apiController,
            credentialsStore: credentialsStore
        )
        
        let output = viewModel.transform(
            LoginViewModelInput(email: Driver.from(optional: "email"),
                                password: Driver.from(optional: "password"),
                                loginButtonTapped: Driver.from(optional: ()),
                                signupButtonTapped: Driver.from(optional: ()),
                                doneTrigger: Driver.from(optional: ()))
        )
        
        XCTAssert(try output.endEditing.toBlocking(timeout: 3).last()! == (), "Editing should be ended")
        XCTAssertTrue(try output.isButtonEnabled.toBlocking(timeout: 3).first()!, "Buttons should be enabled")
        XCTAssertFalse(try output.isLoading.toBlocking(timeout: 3).first()!, "'isLoading' should be 'false'")
        XCTAssertEqual(try output.keyboardHeight.toBlocking(timeout: 3).first(), 0, "Keyboard should be hidden")
    }
    
}

final class LoginRouterTest: BaseRouterTest, LoginRouterType {
    func logIn() {
        debugPrint("login")
    }
}
