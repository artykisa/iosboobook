//
//  PayloadBookTests.swift
//  LibraryTests
//
//  Created by Yan Smaliak on 27.01.2021.
//

import XCTest
@testable import Library

final class PayloadBookTests: XCTestCase {

    func testReadable() throws {
        XCTAssertEqual(PayloadBook.Status.inLibrary.readable, "In Library", "'.inLibrary' in not readable")
        XCTAssertEqual(PayloadBook.Status.pickedUp.readable, "Picked Up", "'.pickedUp' in not readable")
        XCTAssertEqual(PayloadBook.Status.reserved.readable, "Reserved", "'.reserved' in not readable")
    }

}
