//
//  PayloadBook+ExtensionTest.swift
//  LibraryTests
//
//  Created by Yan Smaliak on 29.01.2021.
//

import Foundation
@testable import Library

extension PayloadBook {
    static let empty = PayloadBook(
        id: -1,
        name: "name",
        ownerID: -1,
        createdAt: Date(timeIntervalSince1970: 0),
        updatedAt: Date(timeIntervalSince1970: 0),
        status: .inLibrary,
        deadLine: nil,
        readerUserID: nil
    )
}
